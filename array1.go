package main

import "fmt"

	func main() {
	var pilihan int = 1
	var data []game 
	for pilihan != 0 {
		fmt.Println(`Menu
  1. Input Data Game Baru
  2. Hapus Data Game berdasarkan ID Game
  3. View Data Game beserta jumlah data yang tersimpan
  4. Cari Data Game berdasarkan Nama
  5. Top 3 Game Terfavorit
  6. View Data dengan rating diatas yang diinputkan
  0. Exit`)
		fmt.Print("Silahkan Pilih Menu : ")
		fmt.Scanln(&pilihan)
				switch {
				case 1:
					var judul string
					var rating float32
					fmt.Print("Masukkan Judul Game : ")
					fmt.Scanln(&judul)
					fmt.Print("Masukkan Rating Game (0.0 - 5.0) : ")
					fmt.Scanln(&rating)
					data = addData(data, judul, rating)
					fmt.Println("Data berhasil ditambahkan")
				case 2:
					fmt.Println(viewAllData(data, "List Game \n"))
					var idx int
					fmt.Print("Nomor data yang ingin dihapus : ")
					fmt.Scanln(&idx)
					data = deleteData(data, idx)
					fmt.Println(viewAllData(data, "List Game \n"))
				case 3:
					fmt.Print(viewAllData(data, "List Game \n"))
					fmt.Println("Total Data :", len(data))
				case 4:
					var judul string
					fmt.Print("Masukkan Judul Game : ")
					fmt.Scanln(&judul)
					hasil := searchByJudul(data, judul)
					fmt.Println(hasil)
				case 5:
					top3 := data[0:3]
					fmt.Print(viewAllData(top3, "List Top 3 Game \n"))
				case 6:
					var rate float32
					fmt.Print("Masukkan minimal rating : ")
					fmt.Scanln(&rate)
					hasil := viewDataByRate(data, rate)
					fmt.Println(hasil)
				}
			}
		}}
